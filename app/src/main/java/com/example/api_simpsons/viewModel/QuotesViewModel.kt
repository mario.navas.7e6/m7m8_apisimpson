package com.example.api_simpsons.viewModel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.api_simpsons.model.Quote
import com.example.api_simpsons.model.QuoteItem
import com.example.api_simpsons.model.Repository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class QuotesViewModel : ViewModel() {
    private val repository = Repository()
    var data = MutableLiveData<Quote?>()
    lateinit var currentQuote: QuoteItem
    init {
        fetchData()
    }

    private fun fetchData() {
        CoroutineScope(Dispatchers.IO).launch {
            val response = repository.getQuote()
            withContext(Dispatchers.Main) {
                if (response.isSuccessful) {
                    data.postValue(response.body())
                } else {
                    Log.e("Error :", response.message())
                }
            }
        }
    }

    fun setSelectedQuote(quote: QuoteItem){
        currentQuote = quote
    }







}
