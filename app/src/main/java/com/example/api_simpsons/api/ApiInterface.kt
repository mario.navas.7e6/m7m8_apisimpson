package com.example.api_simpsons.api

import com.example.api_simpsons.model.Quote
import com.example.api_simpsons.model.QuoteItem
import okhttp3.OkHttpClient
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiInterface {
    //Aquí posem les operacions GET,POST, PUT i DELETE vistes abans
    companion object {
        val BASE_URL = "https://thesimpsonsquoteapi.glitch.me/"
        fun create(): ApiInterface {
            val client = OkHttpClient.Builder().build()
            val retrofit = Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()
            return retrofit.create(ApiInterface::class.java)
        }
    }
    @GET("quotes?count=100")
    suspend fun getQuote(): Response<Quote>

    @GET("country/{id}")
    suspend fun getCountry(@Path("id") characterId: Int): Response<QuoteItem>
}
