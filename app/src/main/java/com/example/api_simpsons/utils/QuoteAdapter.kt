package com.example.api_simpsons.utils

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.api_simpsons.model.QuoteItem
import com.example.api_simpsons.view.RecyclerViewFragment
import com.example.m7m8_pr3country.R
import com.example.m7m8_pr3country.databinding.ItemQuoteBinding


class QuoteAdapter(private val countries: List<QuoteItem>, private val listener: RecyclerViewFragment): RecyclerView.Adapter<QuoteAdapter.ViewHolder>(){
    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val binding = ItemQuoteBinding.bind(view)
        fun setListener(country: QuoteItem){
            binding.root.setOnClickListener {
                listener.onClick(country)
            }
        }
    }
    private lateinit var context: Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        val view = LayoutInflater.from(context).inflate(R.layout.item_quote, parent, false)
        return ViewHolder(view)

    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val quote = countries[position]
        with(holder) {
            setListener(quote)
            binding.itemQuoteText.text = quote.quote
            context?.let {
                Glide.with(it)
                    .load(quote.image)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .centerCrop()
                    .circleCrop()
                    .into(binding.itemQuoteImage)
            }
        }


    }

    override fun getItemCount(): Int {
        return countries.size
    }





}