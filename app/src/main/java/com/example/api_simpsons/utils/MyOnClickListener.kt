package com.example.api_simpsons.utils

import com.example.api_simpsons.model.QuoteItem

interface MyOnClickListener {
    fun onClick(quote: QuoteItem)
}