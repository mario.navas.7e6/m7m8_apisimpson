package com.example.api_simpsons.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import com.example.api_simpsons.model.QuoteItem
import com.example.api_simpsons.viewModel.QuotesViewModel
import com.example.m7m8_pr3country.databinding.FragmentDetailBinding
import com.squareup.picasso.Picasso


class DetailFragment : Fragment() {
    lateinit var binding: FragmentDetailBinding
    private val viewModel: QuotesViewModel by activityViewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentDetailBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        binding.detailImage.setImageResource()
        var quote : QuoteItem = viewModel.currentQuote

        if (!quote.image.isNullOrEmpty()) {
            Picasso.get().load(quote.image).into(binding.detailImage)
        }
        binding.name.text = quote.character
        binding.quote.text = quote.quote






//        Glide.with(requireContext())
//            .load(viewModel.currentCountry.media.orthographic)
//            .diskCacheStrategy(DiskCacheStrategy.ALL)
//            .centerCrop()
//            .circleCrop()
//            .into(binding.detailImage)

    }
}

