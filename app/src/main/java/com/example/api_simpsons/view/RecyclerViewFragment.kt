package com.example.api_simpsons.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.api_simpsons.model.QuoteItem
import com.example.api_simpsons.utils.QuoteAdapter
import com.example.api_simpsons.utils.MyOnClickListener
import com.example.api_simpsons.viewModel.QuotesViewModel
import com.example.m7m8_pr3country.R
import com.example.m7m8_pr3country.databinding.FragmentRecyclerViewBinding


class RecyclerViewFragment : Fragment(), MyOnClickListener {
    lateinit var binding: FragmentRecyclerViewBinding
    private lateinit var countryAdapter: QuoteAdapter
    private lateinit var linearLayoutManager: RecyclerView.LayoutManager

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentRecyclerViewBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val viewModel = ViewModelProvider(requireActivity())[QuotesViewModel::class.java]

        viewModel.data.observe(viewLifecycleOwner){
            if (it != null) {
                setUpRecyclerView(it)
            }
        }

    }


    fun setUpRecyclerView(countries: ArrayList<QuoteItem>){
        countryAdapter = QuoteAdapter(countries, this)
        linearLayoutManager = LinearLayoutManager(context)

        binding.recyclerView.apply {
            setHasFixedSize(true) //Optimitza el rendiment de l’app
            layoutManager = linearLayoutManager
            adapter = countryAdapter
        }
    }

    override fun onClick(quote: QuoteItem) {
        val viewModel = ViewModelProvider(requireActivity())[QuotesViewModel::class.java]
        viewModel.setSelectedQuote(quote)
        findNavController().navigate(R.id.action_recyclerViewFragment_to_detailFragment)
    }

}

