package com.example.api_simpsons.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.m7m8_pr3country.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        if(supportActionBar != null){
            supportActionBar?.hide()
        }
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }
}