package com.example.api_simpsons.model

data class Media(
    val emblem: String,
    val flag: String,
    val orthographic: String
)