package com.example.api_simpsons.model

import com.example.api_simpsons.api.ApiInterface


class Repository {
    val apiInterface = ApiInterface.create()

    suspend fun getQuote() = apiInterface.getQuote()
}

