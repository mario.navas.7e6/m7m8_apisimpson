package com.example.api_simpsons.model

data class QuoteItem(
    val quote: String,
    val character: String,
    val image: String,
)
